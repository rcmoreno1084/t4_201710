package view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.Quick;
import model.logic.GeneradorMuestras;
import model.vo.VOPelicula;

public class VistaGeneradorMuestras {
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		boolean fin=false;

		while(!fin){
			printMenu();
			int opcion =sc.nextInt();

			switch (opcion){
			case 1:
				System.out.println("Ingrese el tamaño de la muestra:");
				String cadena = sc.next();
				int i = 0;
				while(i<cadena.length()){
					if (!Character.isDigit(cadena.charAt(i))){
						System.out.println("Número invalido.");
						break;
					}
					i++;
				}
				i = Integer.parseInt(cadena);

				VOPelicula[] muestra;
				try {
					muestra = GeneradorMuestras.generarArreglo("data/movies.csv", i);
					Quick<VOPelicula> a = new Quick<VOPelicula>();
					long ini = System.currentTimeMillis();
					a.sort(muestra);
					long end = System.currentTimeMillis();
					System.out.println("Muestra generada");
					System.out.println("Se ordeno el arreglo usando Quicksort en " + (end-ini) + "ms");
					System.out.println("Ingrese 'Y' para imprimir la muestra, o cualquier tecla para continuar...");
					String verificar = sc.next();
					if(verificar.equals("Y")||verificar.equals("y")){
						for(int j = 0; j<muestra.length;j++){
							System.out.println(muestra[j].getTitulo());
						}
					}
					break;
				} catch (Exception e) {
					System.out.println("Error al cargar el archivo");
					break;
				}
			case 2:
				System.out.println("Ingrese el tama�o de la muestra:");
				String cadenaB = sc.next();
				int k = 0;
				while(k<cadenaB.length()){
					if (!Character.isDigit(cadenaB.charAt(k))){
						System.out.println("Número invalido.");
						break;
					}
					k++;
				}
				k = Integer.parseInt(cadenaB);

				ArrayList<VOPelicula> lista;
				try {
					lista = GeneradorMuestras.generarLista("data/movies.csv", k);
					Quick<VOPelicula> a = new Quick<VOPelicula>();
					long ini = System.currentTimeMillis();
					a.sortList(lista);
					long end = System.currentTimeMillis();
					System.out.println("Muestra generada");
					System.out.println("Se ordeno la lista usando Quicksort en " + (end-ini) + "ms");
					System.out.println("Ingrese 'Y' para imprimir la muestra, o cualquier tecla para continuar...");
					String verificar = sc.next();
					if(verificar.equals("Y")||verificar.equals("y")){
						Iterator<VOPelicula> iter = lista.iterator();
						while(iter.hasNext()) System.out.println(iter.next().getTitulo());
					}
					break;
				} catch (Exception e) {
					System.out.println("Error al cargar el archivo 2");
					break;
				}
			case 3:
				fin = true;
				break;
			}
		}
	}
	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("-Taller 4--Rubén Camilo Moreno---Nicolás Moreno----");
		System.out.println("1. Generar muestra ordenada usando arreglo");
		System.out.println("2. Generar muestra ordenada usando lista");
		System.out.println("3. Salir");
		System.out.println("Por favor ingrese el numero de la opcion a utilizar:");

	}

}
