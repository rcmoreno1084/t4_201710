package model.vo;

import java.util.List;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private String titulo;
	private int agnoPublicacion;
	private List<String> generosAsociados;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public List<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(List<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	@Override
	public int compareTo(VOPelicula arg0) {
		int res = 0;
		String tituloA = titulo +"  ";
		String tituloB = arg0.getTitulo() +"  ";
		int longitudTitulo = tituloA.length();
		int longitudParametro = tituloB.length();
		for(int i = 0;i<longitudTitulo&&i<longitudParametro&& res==0;i++){
			Character a = tituloA.charAt(i);
			Character b = tituloB.charAt(i);
			if(i ==0&&tituloA.startsWith("\"") ){
				int j = i;
				j++;
				a=tituloA.charAt(j);
				longitudTitulo--;
			}
			if(i ==0 &&tituloB.startsWith("\"")){
				int j = i;
				j++;
				b=tituloB.charAt(j);
				longitudParametro--;
			}
			a = Character.toLowerCase(a);
			b= Character.toLowerCase(b);
			
			res = Character.getNumericValue(a) - Character.getNumericValue(b);
		}
		return res;
	}
	

}
