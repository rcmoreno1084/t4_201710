package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import model.vo.VOPelicula;

public class GeneradorMuestras {
	/*
	private int tamanioMuestra;
	private List<T> muestra;
	public GeneradorMuestras(int nMuestra){
		muestra = new ArrayList<T>(nMuestra);
		tamanioMuestra=nMuestra;
	}
	*/
	public static VOPelicula[] generarArreglo(String rArchivo, int nMuestra) throws Exception{
		
		ArrayList muestra = generarLista (rArchivo, nMuestra);
		
		VOPelicula[] res = (VOPelicula[]) muestra.toArray(new VOPelicula [nMuestra]);
		return res;
	}
	
	public static ArrayList<VOPelicula> generarLista(String rArchivo, int nMuestra)throws Exception{
		ArrayList muestra = new ArrayList(nMuestra);
		int tamanioMuestra=nMuestra;

		int agregados=-1;
		Random r = new Random();
		File archivo = new File(rArchivo);
		FileReader fr = new FileReader(archivo);
		BufferedReader br = new BufferedReader(fr);
		
		String linea = br.readLine();
		
		//Verificar que es movies.csv
		if (!linea.contains("genres")){
			muestra = null;
		}
		else{
			//Crea arreglo
			linea = br.readLine();
			while(linea!=null && agregados < tamanioMuestra){
				Long movieId = Long.parseLong(linea.substring(0, linea.indexOf(',')));
				linea = linea.substring(linea.indexOf(',')+1,linea.length());
				
				String generos = linea.substring(linea.lastIndexOf(',')+1,linea.length());
				linea = linea.substring(0,linea.lastIndexOf(','));
				
				//Inicio procesamiento de titulo y agno
				int agno = -1;
				String titulo = linea;
				if(linea.endsWith(")")&& Character.isDigit(linea.charAt(linea.length()-2))||linea.endsWith(")"+"\"")&& Character.isDigit(linea.charAt(linea.length()-3))){
					//System.out.println(linea.substring(linea.lastIndexOf(')')-4,linea.lastIndexOf(')')));
					agno = Integer.parseInt(linea.substring(linea.lastIndexOf(')')-4,linea.lastIndexOf(')')));
					titulo = linea.substring(0, linea.lastIndexOf(' '));
					if(titulo.startsWith("\"")) titulo = titulo+"\"";
				}
				//Fin procesamiento de titulo y agno
				
				List<String> aGeneros = Arrays.asList(generos.split("|"));
				
				VOPelicula add = new VOPelicula();
				add.setTitulo(titulo);
				add.setAgnoPublicacion(agno);
				add.setGenerosAsociados(aGeneros);
				
				//Agregar x numero de veces
				int mayor= 4;
				
				int noVeces = r.nextInt(mayor);
				
				for(int i = 0; i<noVeces && agregados < tamanioMuestra ;i++){
					muestra.add(add);
					agregados++;
				}
				
				linea = br.readLine();
			}
		}
		fr.close();
		br.close();
		
		while(agregados < tamanioMuestra){
			int i = r.nextInt(agregados);			
			muestra.add(muestra.get(i));
			agregados++;
		}
		Collections.shuffle(muestra);
		
		return muestra;
	}
}
