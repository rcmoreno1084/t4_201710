package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada<T> implements ILista<T> {

	//V2
	
	private GenericNode<T> primero;
	private GenericNode<T> ultimo;
	private int posicionActual;
	private int size;

	public ListaDobleEncadenada() {
		primero = null;
		ultimo = null;
	}
	

	@Override
	public Iterator<T> iterator() {
		Iterator<T> it = new Iterator<T>(){
			int index = -1;
			GenericNode<T> currentNode = primero;
			@Override
			public boolean hasNext(){
				if (primero == null || currentNode.darSiguiente() == null){
					return false;
				}else
					return true;
			}
			@Override
			public T next() {
				if (index == -1){
					currentNode = primero;
					index++;
					return (T) primero.darItem();
				}else if (this.hasNext()){
					currentNode = currentNode.darSiguiente();
					index++;
					return (T) currentNode.darItem();
				}else
					return null;
			}
			@Override
			public void remove() {
				if (currentNode == primero){
					primero = primero.darSiguiente();
					primero.setAnterior(null);
				}
				else{
				GenericNode<T> sig = currentNode.darSiguiente();
				GenericNode<T> ant = currentNode.darAnterior();
				currentNode.darAnterior().setSiguiente(sig);
				currentNode.darSiguiente().setAnterior(ant);
				currentNode = currentNode.darAnterior();
				
				}
			}
		};
		return it;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if (primero == null){
			primero = new GenericNode<T>();
			primero.setItem(elem);
			ultimo = primero;
			size = 1;
			posicionActual = 0;
		}else {
			GenericNode<T> add = new GenericNode<T>();
			add.setItem(elem);
			add.setAnterior(ultimo);
			ultimo.setSiguiente(add);
			ultimo= add;
			size++;
			posicionActual++;
		}
		}
			
	

	@Override
	public T darElemento(int pos) {
		int index = -1;
		Iterator<T> iter = this.iterator();
		T res = null;
		while (iter.hasNext()&& index < pos){
			res = iter.next();
			index++;
		}
		if (index < pos-1 || res == null){
			return null;
		}else{
			posicionActual = pos;
			return res;
		}
	}

	@Override
	public int darNumeroElementos() {
		Iterator <T> iter = this.iterator();
		int res = 0;
		while (iter.hasNext()){
			iter.next();
			res++;
		}
		return res;
	}

	
	@Override
	public T eliminarElemento(int pos) {
		int index = -1;
		Iterator<T> iter = this.iterator();
		T res = null;
		while (iter.hasNext()&& index < pos){
			res = iter.next();
			index++;
		}
		if (index < pos-1 || res == null){
			return null;
		}else{
			posicionActual = pos;
			iter.remove();
			return res;
		}
	}

}
