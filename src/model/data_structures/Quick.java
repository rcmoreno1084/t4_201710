package model.data_structures;

import java.util.ArrayList;

public class Quick<T extends Comparable<T>> {

	private T[] values;
	private ArrayList<T> valuesLista;
	private int length;
	private int size;
	
	public void sort(T[] a)
	{
		 if(a == null || a.length == 0)
		 {
			 return;
		 }
		 values = a;
		 length = a.length;
		 quickSort(0, length-1);
	}
	
	public void sortList(ArrayList<T> a)
	{
		if( a == null || a.size() == 0)
		{
			return;
		}
		valuesLista = a;
		size = a.size();
		quickSortLista(0, size-1);
		
	}
	
	private void quickSort(int izquierda, int derecha)
	{
		int i = izquierda;
		int j = derecha;
		T pivot = values[izquierda];
		while(i<= j)
		{
			while(values[i].compareTo(pivot) < 0)
			{
				i++;
			}
			while(values[j].compareTo(pivot) > 0)
			{				
				j--;
			}
			if (i <= j)
			{
				exchangeItems(i, j);
				i++;
				j--;
			}
		}
		if(izquierda < j)
			quickSort(izquierda, j);
		if(i < derecha)
			quickSort(i, derecha);
				
	}
	
	private void exchangeItems(int i, int j)
	{
		T aux = values[i];
		values[i] = values[j];
		values[j] = aux;
	}
	
	private void quickSortLista(int izquierda, int derecha)
	{
		int i = izquierda;
		int j = derecha;
		T pivot = valuesLista.get(izquierda);
		while(i<= j)
		{
			while(valuesLista.get(i).compareTo(pivot) < 0)
			{
				i++;
			}
			while(valuesLista.get(j).compareTo(pivot) > 0)
			{				
				j--;
			}
			if (i <= j)
			{
				exchangeItemsLista(i, j);
				i++;
				j--;
			}
		}
		if(izquierda < j)
			quickSortLista(izquierda, j);
		if(i < derecha)
			quickSortLista(i, derecha);
				
	}
	
	private void exchangeItemsLista(int i, int j)
	{
		T aux = valuesLista.get(i);
		valuesLista.set(i, valuesLista.get(j));
		valuesLista.set(j, aux);
	}
	
}
