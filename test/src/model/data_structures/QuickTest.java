package model.data_structures;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import model.logic.GeneradorMuestras;
import model.vo.VOPelicula;

import org.junit.Test;

public class QuickTest {

	@Test
	public void test() {
		try {
			VOPelicula[] muestra = GeneradorMuestras.generarArreglo("data/movies.csv", 100);
			Quick<VOPelicula> a = new Quick<VOPelicula>();
			a.sort(muestra);
			int i = 0;
			/*
			while(i<muestra.length){
				System.out.println(muestra[i].getTitulo());
				i++;
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLista() {
		try {
			ArrayList<VOPelicula> lista = GeneradorMuestras.generarLista("data/movies.csv", 100);
			Quick<VOPelicula> a = new Quick<VOPelicula>();
			a.sortList(lista);
			Iterator<VOPelicula> iter = lista.iterator();
			while (iter.hasNext()) System.out.println(iter.next().getTitulo());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAleatorio() {
		try {
			
			
			VOPelicula[] muestra = GeneradorMuestras.generarArreglo("data/movies.csv", 1024);
			Quick<VOPelicula> a = new Quick<VOPelicula>();
			long ini = System.currentTimeMillis();
			a.sort(muestra);
			long fin = System.currentTimeMillis();
			
			System.out.println("Duraci�n de ordenamiento de muestra aleatoria: " + (fin-ini)+"ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testAscendente() {
		try {
			VOPelicula[] muestra = GeneradorMuestras.generarArreglo("data/movies.csv", 1024);
			Quick<VOPelicula> a = new Quick<VOPelicula>();
			a.sort(muestra);
			int i =0;
			/*
			while(i<muestra.length){
				System.out.println(muestra[i].getTitulo());
				i++;
			}*/
			long ini = System.currentTimeMillis();
			a.sort(muestra);
			long fin = System.currentTimeMillis();
			
			System.out.println("Duraci�n de ordenamiento de muestra ordenada ascendentemente: " + (fin-ini)+"ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testDescendente() {
		try {
			VOPelicula[] muestra = GeneradorMuestras.generarArreglo("data/movies.csv", 1024);
			Quick<VOPelicula> a = new Quick<VOPelicula>();
			a.sort(muestra);
			java.util.List<VOPelicula> lista = Arrays.asList(muestra);
			Collections.reverse(lista);
			muestra = (VOPelicula[]) lista.toArray();
			int i = 0;
			/*
			while(i<muestra.length){
				System.out.println(muestra[i].getTitulo());
				i++;
			}
			*/
			long ini = System.currentTimeMillis();
			a.sort(muestra);
			long fin = System.currentTimeMillis();
			
			System.out.println("Duraci�n de ordenamiento de muestra ordenada descendentemente: " + (fin-ini)+"ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
